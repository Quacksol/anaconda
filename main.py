#!/usr/bin/env python
import math
import pygame
import random

# TODO Make menu
# TODO Add multiplayer, which uses different buttons to single player
# TODO Fine-tune collision rects (use circles, figure out head polygon)
# TODO Fix spawning Food on player at startup, causing it to collide and game over instantly (add player invincibility?)
# TODO Allow user to change colour
# TODO Change Food's colour depending on score available
# TODO Allow more max foods as an option, more for multiplayer 
# TODO Save preferred game settings to file
# TODO moving food that gives hella points and bounces off objects

# Initialise modules
pygame.init()
pygame.mouse.set_visible(False)
pygame.font.init()
titleFont = pygame.font.SysFont("calibri", int(32))
bigFont = pygame.font.SysFont("calibri", int(16))
smallFont = pygame.font.SysFont("calibri", int(12))

BLACK = pygame.Color(0, 0, 0)
WHITE = pygame.Color(255, 255, 255)
GREEN = pygame.Color(0, 255, 0)
RED = pygame.Color(255, 0, 0)
BLUE = pygame.Color(0, 0, 255)

fps = 60  # Tasty
clock = pygame.time.Clock()

SCREEN_WIDTH = 320
SCREEN_HEIGHT = 240

HEAD_SIZE = 16
BODY_SIZE = 8
ANGLE_INC = 2
BODY_GAP = 16
BASE_MOVE_SPEED = 0.5

MAX_FOODS = 2
FOOD_SIZE = 8

INPUTMODE_SINGLEPLAYER = 0
INPUTMODE_MULTIPLAYER_ONE = 1
INPUTMODE_MULTIPLAYER_TWO = 2

MENU_ICON_SIZE = 8
GAMESTATE_MENU = 0
GAMESTATE_SINGLEPLAYER = 1
GAMESTATE_MULTIPLAYER = 2
GAMESTATE_QUIT = 3

# TODO make a map / dict for controls
GCW_UP = pygame.K_UP
GCW_DOWN = pygame.K_DOWN
GCW_LEFT = pygame.K_LEFT
GCW_RIGHT = pygame.K_RIGHT
GCW_A = pygame.K_LCTRL
GCW_B = pygame.K_LALT
GCW_Y = pygame.K_LSHIFT
GCW_X = pygame.K_SPACE
GCW_L = pygame.K_TAB
GCW_R = pygame.K_BACKSPACE
GCW_SELECT = pygame.K_ESCAPE
GCW_START = pygame.K_RETURN


class AnacondaBody(pygame.sprite.Sprite):
    def __init__(self, colour):
        pygame.sprite.Sprite.__init__(self)
        self.colour = colour
        self.width = self.height = int(BODY_SIZE)
        self.image = pygame.Surface([self.width, self.height])
        pygame.draw.ellipse(self.image, self.colour, [0, 0, self.width, self.height], 1)
        self.rect = self.image.get_rect()

    def set_pos(self, x, y):
        self.rect.centerx = int(x)
        self.rect.centery = int(y)


class Anaconda(pygame.sprite.Sprite):
    def __init__(self, colour):
        pygame.sprite.Sprite.__init__(self)
        self.colour = colour
        self.width = self.height = HEAD_SIZE
        self.baseImage = pygame.Surface([self.width, self.height])
        size = HEAD_SIZE - 1
        mid = int(size / 2) + 1
        if 0:
            pygame.draw.polygon(self.baseImage, self.colour, \
                                [[0, size], [size, size], [size, mid], \
                                 [mid-1, 0], [0, mid]], \
                                1)
        else:
            pygame.draw.polygon(self.baseImage, self.colour, \
                                [[0, 15], [15, 15], [15, 7], \
                                 [8, 0], [7, 0], [0, 7]], \
                                1)
        # TODO set background colour
        self.drawRect = self.baseImage.get_rect()
        self.drawRect.center = (HEAD_SIZE/2, HEAD_SIZE/2)
        self.rect = self.baseImage.get_rect(center = self.drawRect.center)
        self.image = None

        # Input buttons
        self.button_left = None
        self.button_right = None
        self.button_boost = None

        # Absolute Position
        self.x, self.y = [100.0, 200.0]
        self.angle = 0
        self.moveSpeed = BASE_MOVE_SPEED
        self.positionStore = []
        self.bodyBits = pygame.sprite.LayeredUpdates()

        self.score = 0
        self.collGroup = pygame.sprite.Group()
        self.alive = True
        self.deadCounter = 0  # Used when died to delete the bodyBits in a cool way

        for i in range(3):
            self.add_bodyBit()

    def init_inputs(self, mode):
        # Depending on mode, set this object's button inputs to something
        if mode == INPUTMODE_SINGLEPLAYER:
            self.button_left = GCW_LEFT
            self.button_right = GCW_RIGHT
            self.button_boost = GCW_A
        if mode == INPUTMODE_MULTIPLAYER_ONE:
            self.button_left = GCW_LEFT
            self.button_right = GCW_RIGHT
            self.button_boost = GCW_L
        if mode == INPUTMODE_MULTIPLAYER_TWO:
            self.button_left = GCW_Y
            self.button_right = GCW_A
            self.button_boost = GCW_R

    def update(self):
        if self.alive:
            self.image = pygame.transform.rotate(self.baseImage, self.angle)
            self.drawRect = self.image.get_rect(center = self.rect.center)

            dx = -math.sin(math.radians(self.angle)) * self.moveSpeed
            dy = -math.cos(math.radians(self.angle)) * self.moveSpeed
            self.x += dx
            self.y += dy

            self.rect.centerx = int(self.x)
            self.rect.centery = int(self.y)
            self.positionStore.append([self.x, self.y])
            lenny = len(self.positionStore)
            index = lenny - BODY_GAP  # Make the first one start a bit further away
            # First, remove any positionStore values that are too old (from the front of the list)
            if len(self.positionStore) > len(self.bodyBits) * BODY_GAP + BODY_GAP:
                self.positionStore.pop(0)

            # Now set positions of bodyBits to values in the positionStore
            for bodyBit in self.bodyBits:
                index -= BODY_GAP
                if index > 0:
                    bodyBit.set_pos(self.positionStore[index][0], self.positionStore[index][1])
                else:
                    bodyBit.set_pos(self.positionStore[0][0], self.positionStore[0][1])
        else:
            self.deadCounter += 1
            if self.deadCounter >= 3:
                self.deadCounter = 0
                # Delete the bodyBit at the end of the list
                lenny = len(self.bodyBits)
                index = 0
                for bb in self.bodyBits:  # TODO there must be another way
                    index += 1
                    if index == lenny:
                        self.bodyBits.remove(bb)

    def draw(self, screen):
        # We need 2 rects, one for drawing and one for colliding.
        # This function draws the drawRect to the screen, and draws the bodyBits too
        try:
            screen.blit(self.image, [self.drawRect.x, self.drawRect.y])
        except:
            print("Image in None, weird")
        self.bodyBits.draw(screen)

    def handle_input(self, heldButton):
        if heldButton[self.button_left]:
            self.angle += ANGLE_INC
        if heldButton[self.button_right]:
            self.angle -= ANGLE_INC
        if heldButton[self.button_boost]:
            self.moveSpeed = BASE_MOVE_SPEED * 2
        else:
            self.moveSpeed = BASE_MOVE_SPEED

    def update_angle(self, angleInc):
        self.angle += angleInc

    def add_bodyBit(self):
        bodyBit = AnacondaBody(self.colour)
        self.bodyBits.add(bodyBit)
        return bodyBit

    def add_to_collGroup(self, obj):
        if obj is not None:
            self.collGroup.add(obj)

    def set_pos(self, x, y):
        self.x = x
        self.y = y

    def stop(self):
        self.moveSpeed = 0
        self.alive = False


class Food(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.colour = WHITE
        self.width = self.height = int(FOOD_SIZE)
        self.image = pygame.Surface([self.width, self.height])
        pygame.draw.ellipse(self.image, self.colour, [0, 0, self.width, self.height], 1)
        self.rect = self.image.get_rect()
        self.rect.x = random.randint(0, SCREEN_WIDTH-FOOD_SIZE)
        self.rect.y = random.randint(0, SCREEN_HEIGHT-FOOD_SIZE)

        self.score = 200

    def update(self):
        if self.score > 50:
            self.score -= 1


def run_menu(screen):
    exitCode = 0
    titleTextSurface = titleFont.render("ANACONDA", False, WHITE)
    onePlayerTextSurface = bigFont.render("ONE PLAYER", False, WHITE)
    twoPlayerTextFont = bigFont.render("TWO PLAYER", False, WHITE)
    quitTextSurface = bigFont.render("QUIT", False, WHITE)
    menuIcon = pygame.Surface([MENU_ICON_SIZE, MENU_ICON_SIZE])
    pygame.draw.ellipse(menuIcon, WHITE, [0, 0, MENU_ICON_SIZE, MENU_ICON_SIZE], 0)

    menuState = 0

    while exitCode == GAMESTATE_MENU:
        nextMenu = False
        # --- Input logic here
        for pressedButton in pygame.event.get():
            if pressedButton.type == pygame.QUIT:
                exitCode = GAMESTATE_QUIT
            if pressedButton.type == pygame.KEYDOWN:
                if pressedButton.key == GCW_L:  # Change colour left
                    pass
                if pressedButton.key == GCW_R:  # Change colour right
                    pass
                if pressedButton.key == GCW_SELECT:  # Cycle to next menu option
                    menuState += 1
                if pressedButton.key == GCW_DOWN:
                    menuState += 1
                if pressedButton.key == GCW_UP:
                    menuState -= 1
                if pressedButton.key == GCW_START:  # Advance a menu
                    nextMenu = True

        # --- Game logic should go here
        if menuState < 0:
            menuState = 2
        if menuState == 0 and nextMenu:
            exitCode = GAMESTATE_SINGLEPLAYER
        if menuState == 1 and nextMenu:
            exitCode = GAMESTATE_MULTIPLAYER
        if menuState == 2 and nextMenu:
            exitCode = GAMESTATE_QUIT
        if menuState > 2:
            menuState = 0
        # ----------------------------- Group Updates ------------------------------------

        # --- Drawing code should go here
        screen.fill(BLACK)

        # ----------------------------- Other Updates ------------------------------------

        # Lastly, texts
        screen.blit(titleTextSurface, [50, 60])
        screen.blit(onePlayerTextSurface, [100, 128])
        screen.blit(twoPlayerTextFont, [100, 160])
        screen.blit(quitTextSurface, [100, 192])
        screen.blit(menuIcon, [80, 128 + MENU_ICON_SIZE/2 + 32 * menuState])

        # --- Go ahead and update the screen with what we've drawn.
        pygame.display.flip()

    return exitCode


def run_game(screen, gameMode):
    # First get a load of variables from definitions file
    exitCode = gameMode
    done = False
    dt = 0

    totalScore = 0      # Total score for all players, incase you want co-op
    diffScoreMod = 0    # Score mod - reach 1000, increase score (and decrement this by 1000)

    difficulty = 3

    players = pygame.sprite.Group()
    if gameMode == GAMESTATE_SINGLEPLAYER:
        playerOne = Anaconda(GREEN)
        playerOne.init_inputs(INPUTMODE_SINGLEPLAYER)
        playerOne.set_pos(SCREEN_WIDTH/2, SCREEN_HEIGHT/4*3)
        playerOne.update()  # Initial run through
        players.add(playerOne)
    elif gameMode == GAMESTATE_MULTIPLAYER:
        playerOne = Anaconda(GREEN)
        playerOne.init_inputs(INPUTMODE_MULTIPLAYER_ONE)
        playerOne.set_pos(SCREEN_WIDTH/4, SCREEN_HEIGHT/4*3)
        playerOne.update()  # Initial run through
        playerTwo = Anaconda(RED)
        playerTwo.init_inputs(INPUTMODE_MULTIPLAYER_TWO)
        playerTwo.set_pos(SCREEN_WIDTH/4*3, SCREEN_HEIGHT/4*3)
        playerTwo.update()  # Initial run through
        players.add(playerOne)
        players.add(playerTwo)

        # Some initial collisionGroup stuff
        playerOne.add_to_collGroup(playerTwo)
        playerTwo.add_to_collGroup(playerOne)
        if 1:
            for bb in playerOne.bodyBits:
                playerTwo.add_to_collGroup(bb)
            for bb in playerTwo.bodyBits:
                playerOne.add_to_collGroup(bb)
    else:
        print("Invalid Game State!")
        return GAMESTATE_MENU

    foodGroup = pygame.sprite.Group()
    for i in range(MAX_FOODS):
        foodGroup.add(Food())

    # -------- Main Program Loop -----------
    while not done:
        # --- Input logic here
        for pressedButton in pygame.event.get():
            if pressedButton.type == pygame.QUIT:
                done = True
                exitCode = GAMESTATE_QUIT
            if pressedButton.type == pygame.KEYDOWN:
                if pressedButton.key == pygame.K_SPACE:
                    for player in players:
                        bb = player.add_bodyBit()
                        for p in players:
                            p.add_to_collGroup(bb)
                #if pressedButton.key == GCW_L:
                #    difficulty -= 1
                #if pressedButton.key == GCW_R:
                #    difficulty += 1
                if pressedButton.key == GCW_SELECT:
                    done = True
                if pressedButton.key == GCW_START:
                    exitCode = 0
                    done = True

        heldButton = pygame.key.get_pressed()

        for player in players:
            player.handle_input(heldButton)

        # --- Game logic should go here
        # ----------------------------- Collision checks ---------------------------------
        for player in players:
            # Collisions with foods
            col_list = pygame.sprite.spritecollide(player, foodGroup, False)
            for food in col_list:  # TODO too much going on in here
                player.score += food.score
                totalScore += food.score
                diffScoreMod += food.score
                if diffScoreMod >= 1000:
                    diffScoreMod -= 1000
                    difficulty += 1
                for i in range(difficulty):
                    bb = player.add_bodyBit()
                    for p in players:
                        p.add_to_collGroup(bb)
                foodGroup.remove(food)
                foodGroup.add(Food())
            # Collisions with body parts
            col_list = pygame.sprite.spritecollide(player, player.collGroup, False)
            for bodyBit in col_list:
                player.stop()

        # ----------------------------- Group Updates ------------------------------------
        players.update()

        if dt % 4 == 0:  # 4 times a second
            foodGroup.update()

        # --- Drawing code should go here
        screen.fill(BLACK)

        for player in players:  # Cannot use players.draw(), we need to call our own draw function
            player.draw(screen)

        foodGroup.draw(screen)

        # ----------------------------- Other Updates ------------------------------------

        # Lastly, texts
        scoreXDisplacement = 16
        for player in players:
            scoreSurface = bigFont.render("Score: " + str(player.score), False, WHITE)
            screen.blit(scoreSurface, [scoreXDisplacement, 10])
            scoreXDisplacement = 232

        difficultySurface = bigFont.render("Level: " + str(difficulty-2), False, WHITE)
        screen.blit(difficultySurface, [135, 10])

        # --- Go ahead and update the screen with what we've drawn.
        pygame.display.flip()

        # --- Limit to 60 frames per second
        clock.tick(fps)
        dt += 1
        if dt == fps:
            dt = 0
            # print(clock.get_fps())

    return exitCode


if __name__ == '__main__':
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))  # , pygame.FULLSCREEN)
    gameState = 0
    restartApp = 0
    while 1:
        if gameState == GAMESTATE_MENU:      # Menu 1
            gameState = run_menu(screen)
        elif gameState == GAMESTATE_SINGLEPLAYER:    # Game
            gameState = run_game(screen, gameState)
        elif gameState == GAMESTATE_MULTIPLAYER:  # Game
            gameState = run_game(screen, gameState)
        else:                   # Exit game
            break
